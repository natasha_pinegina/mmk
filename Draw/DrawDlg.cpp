﻿
// DrawDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Draw.h"
#include "DrawDlg.h"
#include "afxdialogex.h"
#include "vector"
#include "Drawer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
// Диалоговое окно CDrawDlg



CDrawDlg::CDrawDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DRAW_DIALOG, pParent)
	, a(M_A)
	, T_energy_edit(0)
	, U_energy_edit(0)
	, E_energy_edit(0)
	, v0_max_edit(50)
	, N(100)
	, Nl_edit(30)
	, speedUp_edit(10)
	, dt_edit(0.001*sqrt(M_AR*M_A*M_A/M_D))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDrawDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, a);
	DDX_Text(pDX, IDC_Tedit, T_energy_edit);
	DDX_Text(pDX, IDC_Uedit, U_energy_edit);
	DDX_Text(pDX, IDC_Eedit, E_energy_edit);
	DDX_Text(pDX, IDC_V0edit, v0_max_edit);
	DDX_Text(pDX, IDC_Nedit, N);
	DDX_Text(pDX, IDC_Nledit, Nl_edit);
	DDX_Text(pDX, IDC_EDIT2, speedUp_edit);
	DDX_Text(pDX, IDC_EDIT3, dt_edit);
}

BEGIN_MESSAGE_MAP(CDrawDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CDrawDlg::OnBnClickedOk)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON2, &CDrawDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// Обработчики сообщений CDrawDlg

BOOL CDrawDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	PicWnd_MAX = GetDlgItem(IDC_MAX);
	PicDc_MAX = PicWnd_MAX->GetDC();
	PicWnd_MAX->GetClientRect(&Pic_MAX);

	PicWndParticleMotion = GetDlgItem(IDC_particle_motion);
	PicDcParticleMotion = PicWndParticleMotion->GetDC();
	PicWndParticleMotion->GetClientRect(&PicParticleMotion);

	PicWndMaxwel = GetDlgItem(IDC_Maxwell_pic);
	PicDcMaxwel = PicWndMaxwel->GetDC();
	PicWndMaxwel->GetClientRect(&PicMaxwel);

	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		0.1,						//толщина 1 пиксель
		RGB(0, 0, 250));		//цвет  черный
	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 0));			//цвет черный

	srand(time(0));

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CDrawDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CDrawDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


double DoubleRand(double _max, double _min)
{
	return _min + double(rand()) / RAND_MAX * (_max - _min);
}

void CDrawDlg::OnBnClickedOk()
{
	KillTimer(ID_TIMER_1);
	UpdateData(TRUE);
	Mass.resize(3);
	GraphPen.clear();
	GraphPen.push_back(new CPen(PS_SOLID, 2, RGB(255, 0, 0)));
	GraphPen.push_back(new CPen(PS_SOLID, 2, RGB(0, 255, 0)));
	GraphPen.push_back(new CPen(PS_SOLID, 2, RGB(0, 0, 255)));
	RingedCoordinate<double> buf;
	buf.setMax(Nl_edit);
	scene_.aRadius = a;
	r = 1;
	count = 0;
	scene_.rScale = 1. / M_A;
	scene_.mScale = 1. / M_AR;
	scene_.mass = M_AR;
	scene_.tScale = 1. / dt_edit;
	scene_.dConst = M_D;
	scene_.dScale = 1. / M_D;
	if(!scene_.Init(N))
		return;
	scene_.InitV(v0_max_edit*10.);
	if (v0_max_edit > 0)
		speed_scale = 0.1 * Nl_edit / (scene_.maxVel() * sqrt(2));
	else
		speed_scale = 0;
	for (int i = 0; i < 3; i++)
	{
		Mass[i].clear();
	}
	T_energy_edit = scene_.tEnergy();
	U_energy_edit = scene_.uEnergy();
	E_energy_edit = T_energy_edit + U_energy_edit;
	UpdateData(FALSE);
	Mass[0].push_back(T_energy_edit);
	Mass[1].push_back(U_energy_edit);
	Mass[2].push_back(E_energy_edit);
	maxEnergy = max(T_energy_edit, max(U_energy_edit, E_energy_edit));
	//DrawParticleMotion(scene_.scaleR(0), scene_.scaleR(1), scene_.scaleV(0), scene_.scaleV(1), speed_scale, PicDcParticleMotion, PicParticleMotion);
	DrawParticleMotion(scene_.x, scene_.y, scene_.scaleV(0), scene_.scaleV(1), speed_scale, PicDcParticleMotion, PicParticleMotion);
	if (scene_.Maxwel(((double(N)/5.<15.)?(double(N)/5.) : 15.), maxwel_real, maxwel_th))
	{
		DrawGraph(std::vector<std::vector<double>>({ maxwel_real,maxwel_th }), 0, scene_.maxVel(), GraphPen, PicDcMaxwel, PicMaxwel);
	}
	running = true;
	SetTimer(ID_TIMER_1, 1, NULL);
}

void CDrawDlg::OnTimer(UINT_PTR uTime)
{
	if (!running)
		return;
	for (int i = 0; i < speedUp_edit; i++)
	{
		scene_.Step(dt_edit);
		count++;
		if (count > 50)
		{
			count = 0;
			T_energy_edit = scene_.tEnergy();
			U_energy_edit = scene_.uEnergy();
			E_energy_edit = T_energy_edit + U_energy_edit;
			Mass[0].push_back(T_energy_edit);
			Mass[1].push_back(U_energy_edit);
			Mass[2].push_back(E_energy_edit);
			UpdateData(FALSE);
			if (((T_energy_edit < 3. * maxEnergy) &&
				(U_energy_edit < 3. * maxEnergy) &&
				(E_energy_edit < 3. * maxEnergy)))
			{
				maxEnergy = max(max(maxEnergy, T_energy_edit), max(U_energy_edit, E_energy_edit));
			}
		}
	}
	DrawParticleMotion(scene_.x, scene_.y, scene_.scaleV(0), scene_.scaleV(1), speed_scale, PicDcParticleMotion, PicParticleMotion);
	if (Mass[0].size() >= 3)
	{
		int esize = Mass[0].size();
		if (esize > 60)
			esize = 60;
		auto buf = Mass;
		if(Mass[0].size()>esize)
			for (auto& ref : buf)
				ref.erase(ref.begin(), ref.begin() + (ref.size() - esize+1));

		DrawGraph(buf, 0, maxEnergy, GraphPen, PicDc_MAX, Pic_MAX);
		if (scene_.Maxwel(((double(N) / 5. < 25.) ? (double(N) / 5.) : 25.), maxwel_real, maxwel_th))
		{
			DrawGraph(std::vector<std::vector<double>>({ maxwel_real,maxwel_th }), 0, scene_.maxVel(), GraphPen, PicDcMaxwel, PicMaxwel);
		}
	}
}


void CDrawDlg::OnBnClickedButton2()
{
	// TODO: добавьте свой код обработчика уведомлений
	running = !running;
}


void CDrawDlg::OnBnClickedButton1()
{
	// TODO: добавьте свой код обработчика уведомлений
	running = false;
	KillTimer(ID_TIMER_1);
}
