﻿
// DrawDlg.h: файл заголовка
//

#pragma once
#include <vector>
#include "../Model/loopedscene.h"
#if !defined(AFX_DIADLG_H__EBFF09CD_7067_11D5_B617_89F1B6D3EB46__INCLUDED_)
#define AFX_DIADLG_H__EBFF09CD_7067_11D5_B617_89F1B6D3EB46__INCLUDED_
#endif
#define ID_TIMER_1 100

// Диалоговое окно CDrawDlg
class CDrawDlg : public CDialogEx
{
// Создание
public:
	CDrawDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DRAW_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV
public:
	CPen* pen;
	CPen setka_pen, osi_pen;
	CPen graph1pen;
	CPen graf_pen2;
	CPen graf_pen3;
	CFont fontgraph;

	CWnd* PicWnd_MAX;
	CDC* PicDc_MAX;
	CRect Pic_MAX;

	CWnd* PicWndParticleMotion;
	CDC* PicDcParticleMotion;
	CRect PicParticleMotion;

	CWnd* PicWndMaxwel;
	CDC* PicDcMaxwel;
	CRect PicMaxwel;

	double xminget, xmaxget,
		yminget, ymaxget,
		xpget, ypget;

	afx_msg void OnTimer(UINT_PTR);

// Реализация
protected:
	HICON m_hIcon;
	LoopedScene scene_;
	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	void CDrawDlg::DrawGraph(std::vector<std::vector<double>>& Mass, double MaxX, double MinX, std::vector<CPen*> GraphPen, CDC* WinDc, CRect WinPic);
	afx_msg void OnBnClickedOk();
	struct molecule
	{
		double x;
		double y;
		double Vx;
		double Vy;
	};
	std::vector<molecule> KoordMolecule;
	void CDrawDlg::DrawParticleMotion(std::vector<RingedCoordinate<double>>& x,
		std::vector<RingedCoordinate<double>>& y,
		std::vector<double>& vx,
		std::vector<double>& vy,
		double speed_scale,
		CDC* WinDc, CRect WinPic);

	double a;
	double r = 20;
	int N = 10;
	int count = 0;
	double T_energy_edit;
	double U_energy_edit;
	double E_energy_edit;
	std::vector<std::vector<double>> Mass;
	std::vector<CPen*> GraphPen;
	std::vector<double> maxwel_th, maxwel_real;
	double maxEnergy{ 0 };
	bool running{ false };
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton1();
	double v0_max_edit;
	double speed_scale{ 1 };
	double Nl_edit;
	unsigned int speedUp_edit;
	double dt_edit;
};
