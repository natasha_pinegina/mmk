#pragma once
#include<vector>
#include "pch.h"
#include "DrawDlg.h"

#define KOORDGET(x,y) (xpget*((x)-xminget)),(ypget*((y)-ymaxget)) 

using namespace std;

void CDrawDlg::DrawGraph(std::vector<std::vector<double>>& Mass, double MinX, double MaxX, std::vector<CPen*> GraphPen, CDC* WinDc, CRect WinPic)
{
	//----- ����� ������������� � ������������ �������� -----------------------------

	vector<double> MaxY;
	MaxY.resize(Mass.size());

	vector<double> MinY;
	MinY.resize(Mass.size());

	for (int i = 0; i < Mass.size(); i++)
	{
		for (int j = 0; j < Mass[i].size(); j++)
		{
			if (MaxY[i] < Mass[i][j]) MaxY[i] = Mass[i][j];
			if (MinY[i] > Mass[i][j]) MinY[i] = Mass[i][j];
		}
	}


	xminget = -MaxX / 10;			//����������� �������� �
	xmaxget = MaxX;			//������������ �������� �
	yminget = 0;			//����������� �������� y
	ymaxget = 0;			//������������ �������� y

	for (int i = 0; i < MaxY.size(); i++)
	{
		if (MaxY[i] > ymaxget) ymaxget = MaxY[i];
		if (MinY[i] < yminget) yminget = MinY[i];
	}



	xpget = ((double)(WinPic.Width()) / (xmaxget - xminget));			//������������ ��������� ��������� �� �
	ypget = -((double)(WinPic.Height()) / (ymaxget - yminget));

	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	MemDc->FillSolidRect(WinPic, RGB(255, 255, 255));

	MemDc->SelectObject(&setka_pen);
	double shagX = (xmaxget - xminget) / 10;
	//��������� ����� �� y
	for (float x = xminget; x <= xmaxget; x += shagX)
	{
		MemDc->MoveTo(KOORDGET(x, ymaxget));
		MemDc->LineTo(KOORDGET(x, yminget));
	}
	double shagY = (ymaxget - yminget) / 10;
	//��������� ����� �� x
	for (float y = yminget; y <= ymaxget; y += shagY)
	{
		MemDc->MoveTo(KOORDGET(xminget, y));
		MemDc->LineTo(KOORDGET(xmaxget, y));
	}

	MemDc->SelectObject(&osi_pen);
	//������ ��� Y
	MemDc->MoveTo(KOORDGET(0, ymaxget));
	MemDc->LineTo(KOORDGET(0, yminget));
	//������ ��� �
	MemDc->MoveTo(KOORDGET(xminget, 0));
	MemDc->LineTo(KOORDGET(xmaxget, 0));

	//������� ����
	MemDc->TextOutW(KOORDGET(0, ymaxget - 0.2), _T("")); //Y
	MemDc->TextOutW(KOORDGET(xmaxget - 0.3, 0 - 0.2), _T("A")); //X

	CFont font3;
	font3.CreateFontW(15, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	MemDc->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = yminget; i <= ymaxget; i += shagY)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		MemDc->TextOutW(KOORDGET(0.01, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xminget; j <= xmaxget; j += shagX)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		MemDc->TextOutW(KOORDGET(j, -0.05), str);
	}

	double shag = (MaxX - MinX) / Mass[0].size();
	//MemDc->SelectObject(&graf_pen2);
	for (int i = 0; i < Mass.size(); i++)
	{
		double x = MinX;
		MemDc->SelectObject(GraphPen[i]);
		MemDc->MoveTo(KOORDGET(MinX, Mass[i][0]));
		for (int j = 0; j < Mass[i].size(); j++)
		{
			MemDc->LineTo(KOORDGET(x, Mass[i][j]));
			x += shag;
		}
	}
	//----- ����� �� ����� -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CDrawDlg::DrawParticleMotion(std::vector<RingedCoordinate<double>>& x,
	                              std::vector<RingedCoordinate<double>>& y,
	                              std::vector<double>& vx,
	                              std::vector<double>& vy,
	                              double speed_scale,
	                              CDC* WinDc, CRect WinPic)
{
	xminget = 0;			//����������� �������� �
	xmaxget = Nl_edit;			//������������ �������� �
	yminget = 0;			//����������� �������� y
	ymaxget = Nl_edit;			//������������ �������� y

	xpget = ((double)(WinPic.Width()) / (xmaxget - xminget));			//������������ ��������� ��������� �� �
	ypget = -((double)(WinPic.Height()) / (ymaxget - yminget));

	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	MemDc->FillSolidRect(WinPic, RGB(255, 255, 255));

	MemDc->SelectObject(&setka_pen);
	double shagX = (xmaxget - xminget) / 10;
	//��������� ����� �� y
	for (float x = xminget; x <= xmaxget; x += shagX)
	{
		MemDc->MoveTo(KOORDGET(x, ymaxget));
		MemDc->LineTo(KOORDGET(x, yminget));
	}
	double shagY = (ymaxget - yminget) / 10;
	//��������� ����� �� x
	for (float y = yminget; y <= ymaxget; y += shagY)
	{
		MemDc->MoveTo(KOORDGET(xminget, y));
		MemDc->LineTo(KOORDGET(xmaxget, y));
	}

	MemDc->SelectObject(&osi_pen);
	//������ ��� Y
	MemDc->MoveTo(KOORDGET(0, ymaxget));
	MemDc->LineTo(KOORDGET(0, yminget));
	//������ ��� �
	MemDc->MoveTo(KOORDGET(xminget, 0));
	MemDc->LineTo(KOORDGET(xmaxget, 0));

	//������� ����
	MemDc->TextOutW(KOORDGET(0, ymaxget - 0.2), _T("")); //Y
	MemDc->TextOutW(KOORDGET(xmaxget - 0.3, 0 - 0.2), _T("A")); //X

	CFont font3;
	font3.CreateFontW(15, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	MemDc->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = yminget; i <= ymaxget; i += shagY)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		MemDc->TextOutW(KOORDGET(0.01, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xminget; j <= xmaxget; j += shagX)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		MemDc->TextOutW(KOORDGET(j, -0.05), str);
	}

	CPen sh2;
	sh2.CreatePen(PS_SOLID, 2, RGB(5, 5, 5));
	

	int grad1 = 255 * 3;

	

	for (int i = 0; i < x.size(); i++)
	{
		CPen grad_pen1;
		int c_grad1 = grad1 * double(i) / double(x.size());
		int red = c_grad1 % 255;
		int green = abs(c_grad1 - 254) % 255;
		int blue = abs(c_grad1 - 509) % 255;
		grad_pen1.CreatePen(PS_SOLID, 2, RGB(red, green, blue));
		MemDc->SelectObject(sh2);
		MemDc->Ellipse(KOORDGET(double(x[i]) - 0.5 * r, double(y[i]) - 0.5 * r), KOORDGET(double(x[i]) + 0.5 * r, double(y[i]) + 0.5 * r));
		/*MemDc->SelectObject(grad_pen1);
		MemDc->MoveTo(KOORDGET(double(x[i]), double(y[i])));
		MemDc->LineTo(KOORDGET(double(x[i]) + speed_scale * vx[i], double(y[i]) + speed_scale * vy[i]));*/
	}
	//----- ����� �� ����� -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}
