﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Draw.rc
//
#define IDD_DRAW_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDC_MAX                         1000
#define IDC_                            1001
#define IDC_particle_motion             1001
#define IDC_EDIT1                       1002
#define IDC_Tedit                       1003
#define IDC_Eedit                       1004
#define IDC_Uedit                       1005
#define IDC_BUTTON2                     1007
#define IDC_V0edit                      1008
#define IDC_Nedit                       1009
#define IDC_Nledit                      1010
#define IDC_Maxwell_pic                 1011
#define IDC_EDIT2                       1012
#define IDC_EDIT3                       1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
