#include <functional>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>
#include "LoopedScene.h"

template <typename T>
double RingedCoordinate<T>::max_ = double(1);
double LoopedScene::rScale = double(1);
double LoopedScene::vScale = double(1);
double LoopedScene::tScale = double(1);
double LoopedScene::mScale = double(1);
double LoopedScene::dScale = double(1);
double LoopedScene::aRadius = double(1);
double LoopedScene::dConst = double(1);
double LoopedScene::mass = double(1);
double LoopedScene::fScale = double(1);



template <typename T>
RingedCoordinate<T>::RingedCoordinate(T data)
	: data_(data)
{}
template <typename T>
RingedCoordinate<T>::RingedCoordinate()
	: data_(0)
{}

template <typename T>
RingedCoordinate<T>& RingedCoordinate<T>::ringThis()
{
	/*
	while ((data_ < T(0)) || (data_ > max_))
	{
		if (data_ < T(0))
			data_ += max_;
		else if (data_ > max_)
			data_ -= max_;
	}*/
	if (data_ < T(0))
		data_ += max_;
	else if (data_ > max_)
		data_ -= max_;
	return *this;
}

template <typename T>
RingedCoordinate<T> RingedCoordinate<T>::operator+(const RingedCoordinate<T>& x) const
{
	return RingedCoordinate<T>(data_ + x.data_);
}
template <typename T>
RingedCoordinate<T> RingedCoordinate<T>::operator-(const RingedCoordinate<T>& x) const
{
	return RingedCoordinate<T>(data_ - x.data_);
}
template <typename T>
RingedCoordinate<T> RingedCoordinate<T>::operator*(const RingedCoordinate<T>& x) const
{
	return RingedCoordinate<T>(data_ * x.data_);
}
template <typename T>
RingedCoordinate<T> RingedCoordinate<T>::operator/(const RingedCoordinate<T>& x) const
{
	return RingedCoordinate<T>(data_ / x.data_);
}

template <typename T>
RingedCoordinate<T>& RingedCoordinate<T>::operator+=(const RingedCoordinate<T>& x)
{
	data_ += x.data_;
	return this->ringThis();
}
template <typename T>
RingedCoordinate<T>& RingedCoordinate<T>::operator-=(const RingedCoordinate<T>& x)
{
	data_ -= x.data_;
	return this->ringThis();
}
template <typename T>
RingedCoordinate<T>& RingedCoordinate<T>::operator*=(const RingedCoordinate<T>& x)
{
	data_ *= x.data_;
	return this->ringThis();
}
template <typename T>
RingedCoordinate<T>& RingedCoordinate<T>::operator/=(const RingedCoordinate<T>& x)
{
	data_ /= x.data_;
	return this->ringThis();
}

template <typename T>
RingedCoordinate<T> RingedCoordinate<T>::delta(const RingedCoordinate<T>& x)
{
	T del = data_ - x.data_;
	/*
	while (abs(del) > (max_ / 2.))
	{
		if (del > 0)
			return RingedCoordinate<T>(del - max_);
		else
			return RingedCoordinate<T>(del + max_);
	}*/
	if (abs(del) > (max_ / 2.))
	{
		if (del > 0)
			return RingedCoordinate<T>(del - max_);
		else
			return RingedCoordinate<T>(del + max_);
	}
	return RingedCoordinate<T>(del);
}

template <typename T>
bool RingedCoordinate<T>::operator<(const RingedCoordinate<T>& x) const
{
	return data_ < x.data_;
}
template <typename T>
bool RingedCoordinate<T>::operator>(const RingedCoordinate<T>& x) const
{
	return data_ > x.data_;
}
template <typename T>
bool RingedCoordinate<T>::operator<=(const RingedCoordinate<T>& x) const
{
	return data_ <= x.data_;
}
template <typename T>
bool RingedCoordinate<T>::operator>=(const RingedCoordinate<T>& x) const
{
	return data_ >= x.data_;
}
template <typename T>
bool RingedCoordinate<T>::operator==(const RingedCoordinate<T>& x) const
{
	return data_ == x.data_;
}
template <typename T>
RingedCoordinate<T>::operator T()
{
	return data_;
}
template <typename T>
T RingedCoordinate<T>::data()
{
	return data_;
}

LoopedScene::LoopedScene()
{}

bool LoopedScene::Init(int N)
{

	if (N <= 0)
		return false;
	aRadius *= 2.*rScale;
	mass *= mScale;
	dConst *= dScale;
	fScale = mScale * rScale / (tScale * tScale);

	kT = 0;
	x.resize(N);
	y.resize(N);
	vx = std::vector<double>(N, 0);
	vy = std::vector<double>(N, 0);
	Fx = std::vector<double>(N, 0);
	Fy = std::vector<double>(N, 0);

	std::function<bool(int)> placeIsOk = [this](int i) -> bool
	{
		for (int j = 0; j < i; j++)
		{
			if (double((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j])) <= (1.9*1.9*aRadius * aRadius/4.))
			{
				return false;
			}
		}
		return true;
	};

	x[0] = RingedCoordinate<double>::maxLength() * rand() / double(RAND_MAX);
	y[0] = RingedCoordinate<double>::maxLength() * rand() / double(RAND_MAX);

	for (int i = 1; i < N; i++)
	{
		int cc = 0;
		do
		{
			x[i] = RingedCoordinate<double>::maxLength() * rand() / double(RAND_MAX);
			y[i] = RingedCoordinate<double>::maxLength() * rand() / double(RAND_MAX);
			cc++;
			if (cc > 10000)
				return false;
		} while (!placeIsOk(i));
	}
	for (int i = 0; i < N; i++)
		forces(i);
	return true;
}

void LoopedScene::forces(int i)
{/*
	double mul = aRadius/(2.*pow(2.,1./6.));
	double mid_dif_mul = aRadius * aRadius * aRadius * aRadius * aRadius * aRadius/64.;
	mul = mul * mul * mul * mul * mul * mul;
	Fx[i] = 0;
	Fy[i] = 0;
	for (int j = 0; j < x.size(); j++)
	{
		if (i == j)
			continue;
		auto delx = x[i].delta(x[j]), dely = y[i].delta(y[j]);
		double r2 = delx * delx + dely * dely;
		if ((double(abs(delx)) <= 1000. * std::numeric_limits<double>::epsilon())||
			(double(abs(dely)) <= 1000. * std::numeric_limits<double>::epsilon()))
			continue;
		double mul1 = mul / (r2 * r2 * r2) - 1.;
		double mul2 = r2 * r2 * r2 * r2;
		double K = 1.;
		double mid_dif = 0;
		if (r2 > r2_ * r2_ * aRadius * aRadius/4.)
		{
			continue;
		}
		else if (r2 >= r1_ * r1_ * aRadius * aRadius/4.)
		{
			double rr = sqrt(r2);

			mid_dif = -2. * rr * (r2 * rr - mid_dif_mul);
			K = (rr - r1_ * aRadius/2.) / ((r1_ - r2_) * aRadius/2.);
			mid_dif *= K / ((r1_ - r2_) * aRadius/2.);
			K = 1 - K * K;
			K *= K;
			if (K >= 1.)
			{
				K = 0.9;
			}
			//mid_dif *= sqrt(K);
			mid_dif = 0;
		}*//*
		else if (r2 < aRadius * aRadius / 32.)
		{
			continue;
		}*//*
		Fx[i] += (mul1 * K / mul2 + mid_dif) * delx;
		Fy[i] += (mul1 * K / mul2 + mid_dif) * dely;
	}

	Fx[i] *= kostil * mul * dConst/fScale;
	Fy[i] *= kostil * mul * dConst/fScale;*/

	double sig6 = aRadius / (2. * pow(2, 1. / 6.));
	Fx[i] = 0;
	Fy[i] = 0;
	for (int j = 0; j < Fx.size(); j++)
	{
		double delx = x[i].delta(x[j]), dely = y[i].delta(y[j]);
		if ((abs(delx) < 100. * std::numeric_limits<double>::epsilon()) ||
			(abs(dely) < 100. * std::numeric_limits<double>::epsilon()))
			continue;
		double r2 = delx * delx + dely * dely;
		if (r2 > (r2_ * r2_ * aRadius * aRadius / (4.)))
			continue;
		double r6 = r2 * r2 * r2;
		double res = -6. * sig6 / (r6 * r2) * ( 2.*(sig6 / r6) - 1.);
		double k = 1, kdif = 0;
		if (r2 > (r1_ * r1_ * aRadius * aRadius / (4.)))
		{
			k = (sqrt(r2) - r1_ * aRadius / 2.) / ((r1_ - r2_) * aRadius / 2.);
			kdif = sig6 / r6;
			kdif = -4. * kdif * (kdif - 1.) * k;
			k = 1. - k*k;
			kdif *= k;
			k *= k;
			//kdif /= sqrt(r2);
			kdif = 0;
		}
		Fx[i] += (res * k + kdif) * delx;
		Fy[i] += (res * k + kdif) * dely;
	}
	Fx[i] *= -4. * dConst / fScale;
	Fy[i] *= -4. * dConst / fScale;
}

void LoopedScene::Step(double dt)
{
	dt *= tScale;
	//������� �������� �� ���� ����
	for (int i = 0; i < x.size(); i++)
	{
		x[i] += vx[i] * dt + Fx[i] * dt * dt / (2. * mass);
		y[i] += vy[i] * dt + Fy[i] * dt * dt / (2. * mass);
	}
	maxV = 0;
	//������� ��� � ��������� �� ���� ����
	for (int i = 0; i < x.size(); i++)
	{
		double Fx_old = Fx[i], Fy_old = Fy[i];
		forces(i);
		vx[i] += (Fx[i] + Fx_old) * dt / (2. * mass);
		vy[i] += (Fy[i] + Fy_old) * dt / (2. * mass);
		double vv = sqrt(vx[i] * vx[i] + vy[i] * vy[i]);
		if (vv > maxV)
			maxV = vv;
		vbuffer.push_back(sqrt(vv));
	}
}

double LoopedScene::tEnergy()
{
	double res = 0;
	for (int i = 0; i < vx.size(); i++)
	{
		res += vx[i] * vx[i] + vy[i] * vy[i];
	}
	res = res * mass / (20. * mScale);
	//if (res < 10. * std::numeric_limits<double>::epsilon())
	//	return res;
	double buf =  mScale * res * 20. / (3. * double(x.size()));
	if (buf > 10. * std::numeric_limits<double>::epsilon())
		kT = buf;
	return res;
}

double LoopedScene::uEnergy()
{
	double res = 0;
	for (int i = 0; i < x.size(); i++)
	{
		for (int j = 0; j < x.size(); j++)
		{
			if (i == j)
				continue;
			auto delx = x[i].delta(x[j]), dely = y[i].delta(y[j]);
			double r2 = delx * delx + dely * dely;
			if (r2 < 1000. * std::numeric_limits<double>::epsilon())
				continue;
			r2 = aRadius*aRadius / (r2 * pow(2,2./6.));
			r2 *= r2 * r2;
			res += r2 * r2 - r2;
		}
	}
	double eScale = mScale * rScale * rScale / (tScale * tScale);
	return 4. *1e+3* dConst * kostil / eScale * res / dScale;
}

void LoopedScene::InitV(double Temper)
{
	if (Temper <= 0)
		return;
	double disp = M_K * Temper / (double(vx.size())*mass/mScale);
	vScale = rScale / (tScale);
	std::function<double()> m_rasp = [disp]()->double
	{
		double x = double(rand()) / double(RAND_MAX);
		double y = double(rand()) / double(RAND_MAX);
		//double z = double(rand()) / double(RAND_MAX);
		return disp * sqrt(x * x + y * y /* + z * z*/);
	};
	std::function<double()> rand_grad = []()->double
	{
		return 2. * M_PI * double(rand()) / double(RAND_MAX);
	};
	maxV = 0;
	for (int i = 0; i < vx.size(); i++)
	{
		double fi = rand_grad();
		double v = m_rasp();
		vx[i] = v * cos(fi) * vScale;
		vy[i] = v * sin(fi) * vScale;
		if (v > maxV)
			maxV = v;
	}
}

bool LoopedScene::Maxwel(int quality, std::vector<double>& real, std::vector<double>& theory, double speedlimit)
{
	if (kT == 0)
		return false;
	real.clear();
	theory.clear();
	auto vel = real;
	double max_vel = 0;
	double eScale = mScale * rScale * rScale / (tScale * tScale);
	for (int i = 0; i < vx.size(); i++)
	{
		double vbuf = sqrt(vx[i] * vx[i] + vy[i] * vy[i])/vScale;
		//if ((vbuf < speedlimit)||(speedlimit<0))
		//{
			vel.push_back(vbuf);
		//}
	}
	//�� �������� � ��������
	std::sort(vel.begin(), vel.end(), [](const double& r, const double& l)->bool {return (r < l); });
	max_vel = vel.back();
	int j = 0;
	double v = vel.front();
	double dv = (max_vel - v) / double(quality-1.);
	double max_real = 0;
	double max_th = 0;
	for (int i = 0; i < quality; i++)
	{
		double buf = mass * eScale / (2. * kT * mScale);
		double fv = exp(-v * v * buf);
		buf /= 2. * M_PI;
		buf = sqrt(buf);
		fv = 4. * M_PI * buf * buf * buf * v * v * fv;
		fv = fv * dv;
		/*
		if ((fv < 1.e-3) && (i > quality / 2))
			break;*/
		if (fv > max_th)
			max_th = fv;
		theory.push_back(fv);
		int con = 0;
		std::function<bool(int)> time_to_stop = [=](int j)->bool
		{
			if (j >= vel.size())
				return true;
			if (vel[j] >= (v + dv))
				return true;
			return false;
		};
		while (!time_to_stop(j))
		{
			con++;
			j++;
		}
		double p = double(con) / double(vel.size());
		if (p > max_real)
			max_real = p;
		v += dv;
		real.push_back(p);
	}
	
	double sum = 0;
	for (auto t : theory)
		sum += t;
	sum = 1. / sum;
	for (auto& t : theory)
		t *= sum;
	/*
	sum = 0;
	for (auto p : real)
		sum += p;
	sum = 1. / sum;
	for (auto p : real)
		p *= sum;*/
	/*
	double coef = max_real / max_th;
	for (auto& p : theory)
		p *= coef;*/
	/*
	double tsum = 0, rsum = 0;
	for (int i = 1; i < theory.size(); i++)
	{
		tsum = (theory[i - 1] + theory[i]);
		rsum = (real[i - 1] + real[i]);
	}
	tsum *= dv / 2.;
	rsum *= dv / 2.;
	tsum = 1. / tsum;
	rsum = 1. / rsum;
	for (int i = 0; i < theory.size(); i++)
	{
		theory[i] /= tsum;
		real[i] /= rsum;
	}*/
	if (theory.size() > 0)
		return true;
	else
		return false;
}

std::vector<double> LoopedScene::scaleV(int n)
{
	auto buf = vx;
	if (n > 0)
		buf = vy;
	for (auto& v : buf)
		v /= vScale;
	return buf;
}
std::vector<RingedCoordinate<double>> LoopedScene::scaleR(int n)
{
	auto buf = x;
	if (n > 0)
		buf = y;
	for (auto& r : buf)
		r = double(r) / rScale;
	return buf;
}