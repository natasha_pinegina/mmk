#ifndef LOOPEDSCENE_H
#define LOOPEDSCENE_H

#include <vector>

constexpr auto M_D = 1.648e-21;
constexpr auto M_AR = 6.63352146554608e-24;
constexpr auto M_A = 3.82e-10;
constexpr auto M_K = 1.380649e-23;



using namespace std;

template <typename T = double>

class RingedCoordinate
{
public:
    RingedCoordinate(T data);
    RingedCoordinate();
    //для получения значений от ближайших
    RingedCoordinate<T> operator+(const RingedCoordinate<T>& x) const;
    RingedCoordinate<T> operator-(const RingedCoordinate<T>& x) const;
    RingedCoordinate<T> operator*(const RingedCoordinate<T>& x) const;
    RingedCoordinate<T> operator/(const RingedCoordinate<T>& x) const;

    //для изменения собственного значения
    RingedCoordinate<T>& operator+=(const RingedCoordinate<T>& x);
    RingedCoordinate<T>& operator-=(const RingedCoordinate<T>& x);
    RingedCoordinate<T>& operator*=(const RingedCoordinate<T>& x);
    RingedCoordinate<T>& operator/=(const RingedCoordinate<T>& x);

    //для быстрого доступа для сравнения
    bool operator<(const RingedCoordinate<T>& x) const;
    bool operator>(const RingedCoordinate<T>& x) const;
    bool operator<=(const RingedCoordinate<T>& x) const;
    bool operator>=(const RingedCoordinate<T>& x) const;
    bool operator==(const RingedCoordinate<T>& x) const;

	operator T();

    T data();
    T static maxLength() { return max_; }
    void setMax(double max__) { max_ = max__; }

    RingedCoordinate<T> delta(const RingedCoordinate<T>& x);
private:
    mutable T data_;
    static double max_;
	RingedCoordinate<T>& ringThis();
};


class LoopedScene
{
public:
    LoopedScene();
    vector<double> vx,vy;
    vector<RingedCoordinate<double>> x,y;


	enum vars
	{
		X,
		Y,
		VX,
		VY
	};

    bool Init(int N);
    void InitV(double Temper);

    void Step(double dt);
    double tEnergy();
    double uEnergy();
    bool Maxwel(int quality, std::vector<double>& real, std::vector<double>& theory, double speedlimit=-1);
    std::vector<double> scaleV(int n);
    std::vector<RingedCoordinate<double>> scaleR(int n);
    double maxVel() { return maxV; }

    static double rScale;
    static double vScale;
    static double tScale;
    static double mScale;
    static double dScale;

    static double aRadius;
    static double dConst;
    static double mass;
private:
    std::vector<double> Fx, Fy;
    std::vector<double> vbuffer;
    void forces(int i);
    const double r1_ = 1.15, r2_ = 1.75;
    double kT{ 0 };
    double maxV = 0;
    const double kostil = 1;
    static double fScale;
};

#endif // LOOPEDSCENE_H
